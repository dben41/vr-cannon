﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameStateManager : MonoBehaviour {

    // singleton game manager
    public static GameStateManager instance;
    public static Text ScoreText;
    public static Text TimerText;
    public static Text GameOverText;
    public static int score;
    public static float time;
    public static bool gameOver;
    public bool GameOver { get { return gameOver; } set { gameOver = value; } }


    // Use this for initialization
    void Awake()
    {
        instance = this;
        score = 0;
        time = 60f;
        ScoreText = GameObject.FindGameObjectWithTag("ScoreText").GetComponent<Text>();
        TimerText = GameObject.FindGameObjectWithTag("TimerText").GetComponent<Text>();
        GameOverText = GameObject.FindGameObjectWithTag("GameOverText").GetComponent<Text>();
        TimerText.text = time + "";
        InvokeRepeating("decrementTime", 1.0f, 1.0f);
        gameOver = false;
    }

    public void ResetTime() {
        time = 60;
    }

    public void decrementTime()
    {
        if (time > 0)
        {
            time--;
            TimerText.text = time + "";
        }else{
            TimerText.text = "";
            GameOverText.text = "Game Over";
            gameOver = true;
        }
        
    }

    public void decrementScore()
    {

        score -= 5;
        //user doesn't have to have negative scores
        if (score < 0) score = 0;
        ScoreText.text = "Score: " + score;
        // Debug.Log("Score is: " + score);
    }


    public void incrementScore()
    {

        score++;
        ScoreText.text = "Score: " + score;
       // Debug.Log("Score is: " + score);
    }

    public void incrementTime()
    {

        time +=  7;
        TimerText.text = time + "";
        // Debug.Log("Score is: " + score);
    }

    public void Restart()
    {
        gameOver = false;
        score = 0;
        ResetTime();
        ScoreText.text = "Score: " + score;
        TimerText.text = time + "";
        GameManager.instance.Restart();
        GameOverText.text = "";
    }

    public void BackToMainMenu()
    {
        Application.LoadLevel("DemoScene");
    }

    public void ExitApplication()
    {
        Application.Quit();
    }


}
