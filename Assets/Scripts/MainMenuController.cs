﻿using UnityEngine;
using System.Collections;

public class MainMenuController : MonoBehaviour {
    public void LoadForestFire()
    {
        Application.LoadLevel("Forest");
    }

    public void LoadTowerTopple()
    {
        Application.LoadLevel("TowerTopple");
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}
