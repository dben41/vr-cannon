﻿using UnityEngine;
using System;
using System.Timers;
using System.Collections;

public class GameManager : MonoBehaviour {
    public GameObject targetPrefab;
    public GameObject hourGlass;
    public GameObject skullTarget;
    public GameObject flyTarget;
    //float angle;
    private static System.Random r;
    private IEnumerator spawner;
    private IEnumerator spawnerHourGlass;
    private IEnumerator spawnerSkulls;
    private IEnumerator flyingTarget;


    public static GameManager instance;
    void Awake()
    {
        instance = this;
    }
 

        // Use this for initialization
        void Start () {
        r = new System.Random();
        //angle = UnityEngine.Random.Range(1, 360);
        spawner = SpawnTargets();
        spawnerHourGlass = SpawnHourGlasses();
        spawnerSkulls = SpawnSkulls();
        flyingTarget = SpawnFlyingTargets();
        StartCoroutine(spawner);
        StartCoroutine(spawnerHourGlass);
        StartCoroutine(spawnerSkulls);
        StartCoroutine(flyingTarget);
    }

    void Update()
    {
        if (GameStateManager.instance.GameOver) {
            StopCoroutine(spawner);
            StopCoroutine(spawnerHourGlass);
            StopCoroutine(spawnerSkulls);
            StopCoroutine(flyingTarget);
        }
            
    }

    public  void Restart()
    {
        StopCoroutine(spawner);
        StartCoroutine(spawner);
        StopCoroutine(spawnerHourGlass);
        StopCoroutine(flyingTarget);
        StartCoroutine(spawnerHourGlass);
        StopCoroutine(spawnerSkulls);
        StartCoroutine(spawnerSkulls);
        StartCoroutine(flyingTarget);
    }

    private IEnumerator SpawnTargets()
    {
        while (true)
        {
            SpawnTarget(null, null);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.7f,2.5f));
        }
    }

    private IEnumerator SpawnFlyingTargets()
    {
        while (true)
        {
            SpawnFlyingTarget(null, null);
            yield return new WaitForSeconds(UnityEngine.Random.Range(12.7f, 25.5f));
        }
    }

    private IEnumerator SpawnSkulls()
    {
        while (true)
        {
            SpawnSkull(null, null);
            yield return new WaitForSeconds(UnityEngine.Random.Range(5.1f, 10.5f));
        }
    }

    private IEnumerator SpawnHourGlasses()
    {
        while (true)
        {
            SpawnHourGlass(null, null);
            yield return new WaitForSeconds(UnityEngine.Random.Range(20f, 35f));
        }
    }

    private void SpawnTarget(object source, ElapsedEventArgs e) {
        float radius = UnityEngine.Random.Range(4, 7); //arbitrary intial value
        float angle = r.Next(1,360);//UnityEngine.Random.Range(1, 360);
        float cameraLocationX = Camera.main.transform.position.x;
        float cameraLocationZ = Camera.main.transform.position.z;
        float x = (float) Math.Cos(angle) * radius + cameraLocationX;
        float z = (float) Math.Sin(angle) * radius + cameraLocationZ;
        var target = (GameObject)Instantiate(targetPrefab, new Vector3(x, 3.5f, z), Quaternion.identity);
        target.transform.rotation = Quaternion.LookRotation(target.transform.position - Camera.main.transform.position) * Quaternion.Euler(90, 0, 0); //.Rotate(90, 0, 0);
        UnityEngine.Object.Destroy(target, UnityEngine.Random.Range(7, 9));
    }

    bool alternate = false;
    private void SpawnFlyingTarget(object source, ElapsedEventArgs e)
    {
        GameObject target = null;  
        if (alternate)
        {
            target = (GameObject)Instantiate(flyTarget, new Vector3(1318, 9.4f, 1019), Quaternion.identity);
            alternate = false;
        }
        else
        {
            target = (GameObject)Instantiate(flyTarget, new Vector3(1318, 9.4f, 1030), Quaternion.identity);
            alternate = true;
        }
        target.transform.rotation = Quaternion.Euler(90, 0, 0);
        UnityEngine.Object.Destroy(target, 13);
    }

    private void SpawnSkull(object source, ElapsedEventArgs e)
    {
        float radius = UnityEngine.Random.Range(4, 7); //arbitrary intial value
        float angle = r.Next(1, 360);//UnityEngine.Random.Range(1, 360);
        float cameraLocationX = Camera.main.transform.position.x;
        float cameraLocationZ = Camera.main.transform.position.z;
        float x = (float)Math.Cos(angle) * radius + cameraLocationX;
        float z = (float)Math.Sin(angle) * radius + cameraLocationZ;
        var target = (GameObject)Instantiate(skullTarget, new Vector3(x, 3.5f, z), Quaternion.identity);
        target.transform.rotation = Quaternion.LookRotation(target.transform.position - Camera.main.transform.position) * Quaternion.Euler(90, 0, 0); //.Rotate(90, 0, 0);
        UnityEngine.Object.Destroy(target, UnityEngine.Random.Range(4, 7));
    }

    private void SpawnHourGlass(object source, ElapsedEventArgs e)
        {
            float radius = UnityEngine.Random.Range(9, 10); //arbitrary intial value
            float angle = r.Next(1, 360);//UnityEngine.Random.Range(1, 360);
            float cameraLocationX = Camera.main.transform.position.x;
            float cameraLocationZ = Camera.main.transform.position.z;
            float x = (float)Math.Cos(angle) * radius + cameraLocationX;
            float z = (float)Math.Sin(angle) * radius + cameraLocationZ;
            var target = (GameObject)Instantiate(hourGlass, new Vector3(x, 5.5f, z), Quaternion.identity);
            target.transform.rotation = Quaternion.LookRotation(target.transform.position - Camera.main.transform.position) * Quaternion.Euler(90, 0, 0); //.Rotate(90, 0, 0);
            UnityEngine.Object.Destroy(target, UnityEngine.Random.Range(18, 22));
        }

    }
