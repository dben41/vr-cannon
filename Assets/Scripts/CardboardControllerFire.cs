﻿using UnityEngine;
using System.Collections;

public class CardboardControllerFire : MonoBehaviour {
    public GameObject bulletPrefab;

    void Update()
    {
        if (Cardboard.SDK.Triggered)
        {
            this.GetComponent<AudioSource>().Play();
            Fire();
        }
    }

    void Fire()
    {
        bulletPrefab.transform.localScale = CannonSelectorController.instance.getSize();
        bulletPrefab.GetComponent<Rigidbody>().mass = CannonSelectorController.instance.getMass();
        //        Explosion.GetComponent<ExplosionPhysicsForce>().explosionForce = CannonSelectorController.instance.getImpactForce();

        // create the bullet object from the bullet prefab
        var bullet = (GameObject)Instantiate(bulletPrefab, Camera.main.transform.position - Camera.main.transform.forward, Quaternion.identity);

        // make the bullet move away in front of the player
        bullet.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * CannonSelectorController.instance.getVelocity();

        // make bullet disappear after 2 seconds
        Destroy(bullet, 5.0f);
    }
}
