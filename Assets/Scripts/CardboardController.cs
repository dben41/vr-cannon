﻿using UnityEngine;
using System.Collections;

public class CardboardController : MonoBehaviour {
    public GameObject bulletPrefab;

    // Use this for initialization
    void Start () {
	
	}

    void Update()
    {
        if (Cardboard.SDK.Triggered && !GameStateManager.instance.GameOver)
        {
            this.GetComponent<AudioSource>().Play();
            Fire();
        }
        //else if (Input.GetMouseButtonDown(0))
        //{
            //Fire();
        //}


       
    }

    void Fire()
    {
        // create the bullet object from the bullet prefab
        var bullet = (GameObject)Instantiate(bulletPrefab, Camera.main.transform.position - Camera.main.transform.forward, Quaternion.identity);

        // make the bullet move away in front of the player
        bullet.GetComponent<Rigidbody>().velocity = Camera.main.transform.forward * 9;

        // make bullet disappear after 2 seconds
        Destroy(bullet, 3.0f);
    }
}
