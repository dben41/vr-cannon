﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CannonSelectorController : MonoBehaviour {
    enum gunTypes
    {
        Light,
        Medium,
        Heavy,
        Multi,
        Ultra
    };

    //cannonball variables
    float rateOfFire, mass, velocity, impactForce;
    Vector3 size;
    gunTypes stateOfGame;

    public static CannonSelectorController instance;

    void Awake()
    {
        instance = this;
        //initialize
        stateOfGame = gunTypes.Light;
        ToggleCannon();
        


    }

    public void ToggleCannon()
    {
        switch (stateOfGame)
        {
            case gunTypes.Light:
                rateOfFire = 0.0f;
                mass = 0.5f;
                velocity = 20f;
                impactForce = 0.25f;
                size = new Vector3(0.5f, 0.5f, 0.5f);
                stateOfGame = gunTypes.Medium;
                Debug.Log("Toggle Light");
                GameObject.FindGameObjectWithTag("CannonText").GetComponent<Text>().text = "Light Cannon";
                break;
            case gunTypes.Medium:
                rateOfFire = 0.0f;
                mass = 1.5f;
                velocity = 18f;
                impactForce = 0.63f;
                size = new Vector3(0.6f, 0.6f, 0.6f);
                stateOfGame = gunTypes.Heavy;
                Debug.Log("Toggle Medium");
                GameObject.FindGameObjectWithTag("CannonText").GetComponent<Text>().text = "Medium Cannon";
                break;
            case gunTypes.Heavy:
                rateOfFire = 0.0f;
                mass = 5f;
                velocity = 16f;
                impactForce = 1.1f;
                size = new Vector3(1f, 1f, 1f);
                stateOfGame = gunTypes.Ultra;
                Debug.Log("Toggle Heavy");
                GameObject.FindGameObjectWithTag("CannonText").GetComponent<Text>().text = "Heavy Cannon";
                break;
            /*
             * Not yet implemented
             */
            case gunTypes.Multi:
                rateOfFire = 0.0f;
                mass = 0.5f;
                velocity = 13f;
                impactForce = 0.29f;
                size = new Vector3(0.5f, 0.5f, 0.5f);
                stateOfGame = gunTypes.Ultra;
                Debug.Log("Toggle Multi");
                break;
            case gunTypes.Ultra:
                rateOfFire = 0.0f;
                mass = 30f;
                velocity = 20;
                impactForce = 20f;
                size = new Vector3(1.2f, 1.2f, 1.2f);
                stateOfGame = gunTypes.Light;
                Debug.Log("Toggle Ultra");
                GameObject.FindGameObjectWithTag("CannonText").GetComponent<Text>().text = "Ultra Cannon";
                break;
        }
    }


    public float getImpactForce()
    {
        return impactForce;
    }

    public float getMass()
    {
        return mass;
    }

    public Vector3 getSize()
    {
        return size;
    }

    public float getVelocity()
    {
        return velocity;
    }

}
