﻿using UnityEngine;
using System.Collections;

public class TimeText : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        this.GetComponent<AudioSource>().Play();
        Destroy(this.gameObject, 1.25f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(new Vector3(0, 0.1f, 0));
    }
}
