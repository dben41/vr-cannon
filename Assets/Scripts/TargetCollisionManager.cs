﻿using UnityEngine;
using System.Collections;

public class TargetCollisionManager : MonoBehaviour {
    public GameObject Explosion;
    public GameObject plusOneText;

    /*
     * When cannonball hits the target
     */
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Cannonball")
        {
            Debug.Log("TAG: " + this.gameObject.tag);
            Destroy(collisionInfo.gameObject);
            Instantiate(plusOneText, this.transform.position, Quaternion.identity);
            if(this.gameObject.tag == "Skull Target")
            {
                GameStateManager.instance.decrementScore();
            } else if(this.gameObject.tag == "Flying Target")
            {
                GameStateManager.instance.incrementScore();
                GameStateManager.instance.incrementScore();
                GameStateManager.instance.incrementScore();
                GameStateManager.instance.incrementScore();
                GameStateManager.instance.incrementScore();
            }
            else
            {
                GameStateManager.instance.incrementScore();
            }
            
            var explode = Instantiate(Explosion, this.transform.position, Quaternion.identity);       
            Destroy(this.gameObject);
            Destroy(explode, 0.5f);
        }
    }

}
