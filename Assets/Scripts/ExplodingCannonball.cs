﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Effects;

public class ExplodingCannonball : MonoBehaviour {
    public GameObject Explosion;


    void OnCollisionEnter(Collision collisionInfo)
    {
        /*
        Component[] allComponents = Explosion.GetComponents<Component>();
        foreach(Component e in allComponents)
        {
            Debug.Log(e);
        }
        */

        this.GetComponent<AudioSource>().Play();
        Explosion.GetComponent<ExplosionPhysicsForce>().explosionForce = CannonSelectorController.instance.getImpactForce();
        var explosion = Instantiate(Explosion, this.transform.position, Quaternion.identity);
        Destroy(explosion, 0.3f);
        //delay a little bit so that the sound can get initialized
        Destroy(this.gameObject, 0.1f);
    }
}
