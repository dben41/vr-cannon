﻿using UnityEngine;
using System.Collections;

public class HourGlassCollisionManager : MonoBehaviour {

    public GameObject Explosion;
    public GameObject plusOneText;

    /*
     * When cannonball hits the target
     */
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Cannonball")
        {
            //Debug.Log("HIT!");
            Destroy(collisionInfo.gameObject);
            Instantiate(plusOneText, this.transform.position, Quaternion.identity);
            
            GameStateManager.instance.incrementTime();
            var explode = Instantiate(Explosion, this.transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            Destroy(explode, 0.5f);
        }
    }
}
