# VR Cannon README #

[App Store](https://play.google.com/store/apps/details?id=com.Gitmo.CannonVR)

[Game Instructions](http://student-147.coe.utah.edu/cannon.html). 

### Instructions for Building and Deploying VR Cannon
The build machine must have the following software installed:
* Microsoft Windows 7 or greater
* Microsoft Visual Studio 2013 Community or greater
    - https://www.visualstudio.com/en-us/visual-studio-homepage-vs.aspx
* Android Studio
    - http://developer.android.com/sdk/installing/index.html
* Java JDK 7 or greater (32-bit or 64-bit)
    - http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
* Unity 5.3.1 (32-bit)
    - http://download.unity3d.com/download_unity/cc9cbbcc37b4/Windows32EditorInstaller/UnitySetup32-5.3.1f1.exe
* After installing Unity, run the Unity Download Assistant 5.3.1f1
    - http://download.unity3d.com/download_unity/cc9cbbcc37b4/UnityDownloadAssistant-5.3.1f1.exe
    - Uncheck all the items except for 
        - Microsoft Visual Studio Tools for Unity
        - Android Build Support

### Enable Android Developer Options
* Connect an Android device to your computer.
* Instructions and drivers may vary per device manufactuer. Below are general instructions for enabling Developer Options in Android.
    * http://www.howtogeek.com/129728/how-to-access-the-developer-options-menu-and-enable-usb-debugging-on-android-4.2
    * You may have to search your device model online for additional instructions.
    * Test developer functionality by opening a command line window and navigating to your <sdk/platform-tools> directory (e.g. `E:/Appdata/Local/Android/sdk/platform-tools`)
    * Type in the command `adb devices`. If successful, you will see the Android device serial number be listed.

### Configure Unity
Within Unity open the project folder that was previously extracted.
* Go to Edit > Preferences > External Tools. Fill in the following:
    - External Script Editor - \<Select Visual Studio 20XX from the dropdown\>
    - SDK - \<folder path to Android SDK\> (e.g. `E:/Appdata/Local/Android/sdk`)
    - JDK - \<folder path to Java JDK\> (e.g. `C:/Program Files/Java/jdk1.8.0_71`)

### Deploying VR Cannon to Android Device    
Go to File > Build Settings
- Make sure all three scenes are selected (Default, cardboard & main menu)
- Select Android from the build settings and click on Build.
- Select an output directory for the APK.
- The APK will now build, proceed to next step if the build is successful, otherwise, review any compile errors.
- With the Android device connected, select "Build and Run". This will build the APK and also deploy it onto the device.
    - For debugging scripts from Android device, the "Development Build" option should be checked before building.

### Viewing Source Code
Within Unity, go to the Projects window, and expand the Scripts folder.
- Double-click on any script file. It will automatically launch the Unity solution in Visual Studio.
        - In order to properly debug the source code you must always open the Scripts within Unity.

### Debugging Notes
- The recommended debug method is in Unity's "Play" mode. Debugging in play mode significantly decreases the amount of time spent waiting for the code to build and compile for a specific device.
- Within Visual Studio, change the Build setting from "Attach to Unity" to "Attach to Unity and Play"

### Debug Build
- Go to Edit > Project Setings > Player > Android Settings > Publishing Settings
        - Change alias to "Unsigned (debug)"

### Play Store Release Build

- Go to Edit > Project Setings > Player > Android Settings > Other Settings
        - Increase the Version and Bundle Version Code numbers
    - Go to Edit > Project Setings > Player > Android Settings > Publishing Settings
        - Select "Use Existing Keystore"
        - Select the private keystore file (*.keystore)
        - Select the alias and enter in the required passwords
        - Save your project (Ctrl+S)
        - Build APK. This signed version can now be uploaded.